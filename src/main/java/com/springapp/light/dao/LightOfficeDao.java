/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapp.light.dao;

import com.springapp.light.domain.LightOffice;
import com.springapp.light.domain.LightOfficePower;
import com.springapp.light.domain.LightOfficeSize;
import com.springapp.light.domain.LightOfficeType;
import java.util.List;


public interface LightOfficeDao {
    
    List<LightOffice> getListLightOffice();
    List<LightOffice> getListLightOffice(String emergency, String [] arrPowers, String size, String type);
    LightOffice getLightByUrl(String url);
    LightOffice getLightById(String id);

    void saveLightOffice(LightOffice lightOffice);

    List<LightOfficePower> getListLightOfficePower();
    List<LightOfficeSize> getListLightOfficeSize();
    List<LightOfficeType> getListLightOfficeType();

    void  renewLightOfficePower();
    void  renewLightOfficeSize();
    void  renewLightOfficeType();

    List<LightOffice> getListLightByIds(String [] ids);
    List<LightOffice> getListLightFromSearch(String word);
    
}
